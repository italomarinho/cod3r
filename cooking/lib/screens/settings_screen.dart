import 'package:flutter/material.dart';

import '../components/main_drawer.dart';
import '../models/settings.dart';

class SttingsScreen extends StatefulWidget {
  final Settings settings;
  final Function(Settings) onSettingsChanged;

  const SttingsScreen(this.settings, this.onSettingsChanged);

  @override
  _SttingsScreenState createState() => _SttingsScreenState();
}

class _SttingsScreenState extends State<SttingsScreen> {
  late Settings settings;

  @override
  void initState() {
    super.initState();

    settings = widget.settings;
  }

  Widget _createSwitch({
    required String title,
    required String subtitle,
    required bool value,
    required ValueChanged<bool> onChanged,
  }) {
    return SwitchListTile.adaptive(
      title: Text(title),
      subtitle: Text(subtitle),
      value: value,
      onChanged: (value) {
        onChanged(value);
        widget.onSettingsChanged(settings);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Configurações'),
        centerTitle: true,
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.all(20),
            child: Text(
              'Configurações',
              style: Theme.of(context).textTheme.headline6,
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _createSwitch(
                  title: 'Sem Glutén',
                  subtitle: 'Só exibe refeições sem glutén!',
                  value: settings.isGlutenFree,
                  onChanged: (value) {
                    setState(() => settings.isGlutenFree = value);
                  },
                ),
                _createSwitch(
                  title: 'Sem Lactose',
                  subtitle: 'Só exibe refeições sem lactose!',
                  value: settings.isLactoseFree,
                  onChanged: (value) {
                    setState(() => settings.isLactoseFree = value);
                  },
                ),
                _createSwitch(
                  title: 'Vegana',
                  subtitle: 'Só exibe refeições veganas!',
                  value: settings.isVegan,
                  onChanged: (value) {
                    setState(() => settings.isVegan = value);
                  },
                ),
                _createSwitch(
                  title: 'Vegetariana',
                  subtitle: 'Só exibe refeições vegetarianas!',
                  value: settings.isVegetarian,
                  onChanged: (value) {
                    setState(() => settings.isVegetarian = value);
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
